var ActionButtonComponent = ActionComponent.extend({
    _docstring: function (){
        return "Button Component that triggers a server action when clicked";
    },
    draw: function() {
        var myself = this;
        var b = $("<button type='button'/>").text(this.label).unbind("click").bind("click", function(){
            return myself.triggerAction.apply(myself);
        });
        if (typeof this.buttonStyle === "undefined" || this.buttonStyle === "themeroller")
            b.button();
        b.appendTo($("#"+ this.htmlObject).empty());
    }

});
